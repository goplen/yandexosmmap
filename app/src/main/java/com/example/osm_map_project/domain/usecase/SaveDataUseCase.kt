package com.example.osm_map_project.domain.usecase

import com.example.osm_map_project.domain.repository.SharedGetData

class SaveDataUseCase(private val save: SharedGetData) {
    fun execute(): Boolean {
        return save.saveData()
    }
}