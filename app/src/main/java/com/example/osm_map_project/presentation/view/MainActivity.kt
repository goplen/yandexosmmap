package com.example.osm_map_project.presentation.view

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.osm_map_project.R
import com.example.osm_map_project.databinding.ActivityMainBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay


class MainActivity : AppCompatActivity() {
    private lateinit var map: MapView
    private lateinit var mLocationOverlay: MyLocationNewOverlay
    private lateinit var binding: ActivityMainBinding
    private lateinit var fused: FusedLocationProviderClient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fused = LocationServices.getFusedLocationProviderClient(this)

        map = binding.map

        map.setTileSource(TileSourceFactory.MAPNIK)

        Configuration.getInstance().load(getApplicationContext(),
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));

        getPermission()

    }

    private fun getLocation() {
        this.mLocationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(this), map) // Ставит твою текущую локацию
        val mapController = map.controller // устанавливает мап контроллер
        mapController.setZoom(20.0) // Зум
        this.mLocationOverlay.enableMyLocation()
        mLocationOverlay.enableFollowLocation()
        map.getOverlays().add(this.mLocationOverlay)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        val location: Task<Location> = fused.lastLocation
        location.addOnCompleteListener {
            val currentLocation = GeoPoint(it.result.latitude, it.result.longitude)
            mapController.animateTo(currentLocation)
            setMarker(it.result.latitude, it.result.longitude)
        }



    }

    private fun setMarker(latitude: Double, longitude: Double) {
        val startPoint = GeoPoint(latitude, longitude) // Кординаты маркера
        val startMarker = Marker(map) // Установка на карту
        startMarker.position = startPoint // Ставим корды маркера
        startMarker.title = "Тут находится достопримечательность" // Текст устаналивалет на маркет
        startMarker.image = resources.getDrawable(R.drawable.ic_launcher_background)

        map.getOverlays().add(startMarker)

        val listNum = listOf<Int>(11,15,4,13,5,6,8,7,9,10).sorted()
        Log.d("sorted", "$listNum")
    }



    private fun getPermission() {
        Dexter.withContext(this).withPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ).withListener(object : MultiplePermissionsListener{
            override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                getLocation()
            }

            override fun onPermissionRationaleShouldBeShown(
                p0: MutableList<PermissionRequest>?,
                p1: PermissionToken?
            ) {

            }
        }).check()
    }
}