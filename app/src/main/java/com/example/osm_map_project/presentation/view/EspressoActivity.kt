package com.example.osm_map_project.presentation.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.osm_map_project.R
import com.example.osm_map_project.databinding.ActivityEspressoBinding

class EspressoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityEspressoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding  = ActivityEspressoBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}