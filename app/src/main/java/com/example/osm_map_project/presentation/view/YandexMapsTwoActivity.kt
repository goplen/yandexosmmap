package com.example.osm_map_project.presentation.view

import android.Manifest
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.osm_map_project.databinding.ActivityYandexMapsTwoBinding
import com.example.osm_map_project.presentation.view.FragmentBehavior.FirstFragment
import com.example.osm_map_project.presentation.viewModel.YandexMapsViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.RequestPoint
import com.yandex.mapkit.RequestPointType
import com.yandex.mapkit.directions.DirectionsFactory
import com.yandex.mapkit.directions.driving.*
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.layers.ObjectEvent
import com.yandex.mapkit.location.Location
import com.yandex.mapkit.location.LocationListener
import com.yandex.mapkit.location.LocationStatus
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.MapObjectCollection
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.user_location.UserLocationLayer
import com.yandex.mapkit.user_location.UserLocationObjectListener
import com.yandex.mapkit.user_location.UserLocationView
import com.yandex.runtime.Error
import org.koin.androidx.viewmodel.ext.android.viewModel

class YandexMapsTwoActivity : AppCompatActivity(), UserLocationObjectListener, LocationListener {

    //
    private lateinit var binding: ActivityYandexMapsTwoBinding
    private lateinit var fused: FusedLocationProviderClient
    private lateinit var currentLocation: Point
    private lateinit var mapView: MapView
    private lateinit var drivingRouter: DrivingRouter
    private lateinit var userLocationLayer: UserLocationLayer
    //

    //
    private var mapMarker: MapObjectCollection?=null
    private var mapRoute: MapObjectCollection?=null
    private val vm: YandexMapsViewModel by viewModel()
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MapKitFactory.setApiKey("07488be4-c2be-4756-9911-060ba936e370")
        MapKitFactory.initialize(this)

        binding = ActivityYandexMapsTwoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fused = LocationServices.getFusedLocationProviderClient(this)
        mapView = binding.mapMain

        mapMarker = mapView.map.mapObjects.addCollection()
        mapRoute = mapView.map.mapObjects.addCollection()

        supportFragmentManager.beginTransaction().replace(binding.behaviorId.fragmentChainger.id, FirstFragment()).commit()

        val bottomSheetBehavior = BottomSheetBehavior.from(binding.behaviorId.behaviod)
        binding.behaviorId.fragmentChainger.alpha = 0.2f
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback(){
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Log.d("state_beg", "$newState")
                if (newState == 3) {
                    binding.behaviorId.fragmentChainger.alpha = 1f
                } else {
                    binding.behaviorId.fragmentChainger.alpha = 0.2f
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })

        getPermission()
    }

    override fun onStart() {
        super.onStart()
        MapKitFactory.getInstance().onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        MapKitFactory.getInstance().onStop()
        mapView.onStop()
    }

    private fun getPermission() {
        Dexter.withContext(this).withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION)
            .withListener(object: MultiplePermissionsListener{
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    getLocation()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {

                }
            }).check()
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        userLocationLayer = MapKitFactory.getInstance().createUserLocationLayer(mapView.mapWindow)
        userLocationLayer.isVisible = true

        fused.lastLocation.addOnSuccessListener {
            currentLocation = Point(it.latitude, it.longitude)
            movieCameraLocation()
        }
    }

    private fun drawMarkerOnMap(location: Point) {
        mapView.map.mapObjects.addPlacemark(location).addTapListener { p0, p1 ->
            Toast.makeText(this@YandexMapsTwoActivity, "Тут какой-то текст", Toast.LENGTH_SHORT)
                .show()

            true
        }
    }

    private fun doRoute(endPoint: Point) {
        drivingRouter = DirectionsFactory.getInstance().createDrivingRouter()

        val drivingSession = object : DrivingSession.DrivingRouteListener{
            override fun onDrivingRoutes(p0: MutableList<DrivingRoute>) {
                p0.forEach {
                    mapRoute!!.addPolyline(it.geometry)
                }
            }

            override fun onDrivingRoutesError(p0: Error) {

            }
        }

        drawRoute(endPoint, drivingSession)
    }

    private fun drawRoute(endPoint: Point, drivingSession: DrivingSession.DrivingRouteListener) {
        val vehicleOptions = VehicleOptions()
        val drivingOptions = DrivingOptions()
        drivingOptions.routesCount = 1
        drivingOptions.avoidTolls = true
        val requestPoint = ArrayList<RequestPoint>()

        requestPoint.add(
            RequestPoint(
                currentLocation,
                RequestPointType.WAYPOINT,
                null
            )
        )
        requestPoint.add(
            RequestPoint(
                endPoint,
                RequestPointType.WAYPOINT,
                null
            )
        )
        drivingRouter.requestRoutes(requestPoint, drivingOptions, vehicleOptions, drivingSession)
    }

    private fun movieCameraLocation() {
        mapView.map.move(CameraPosition(currentLocation, 15f,0f,0f), Animation(Animation.Type.SMOOTH, 1f), null)
    }

    override fun onObjectAdded(p0: UserLocationView) {

    }

    override fun onObjectRemoved(p0: UserLocationView) {

    }

    override fun onObjectUpdated(p0: UserLocationView, p1: ObjectEvent) {

    }

    override fun onLocationUpdated(p0: Location) {
        currentLocation = Point(p0.position.latitude, p0.position.longitude)
        movieCameraLocation()
    }

    override fun onLocationStatusUpdated(p0: LocationStatus) {
        Toast.makeText(this, "Локация обновлена", Toast.LENGTH_SHORT).show()
    }
}