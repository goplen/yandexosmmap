package com.example.osm_map_project.di

import android.content.Context
import com.example.osm_map_project.common.AuthInterceptor
import com.example.osm_map_project.data.repository.SharedRepositoryImpl
import com.example.osm_map_project.data.storage.SharedRepositoryRepo
import com.example.osm_map_project.data.storage.SharedRepositoryStorage
import com.example.osm_map_project.domain.repository.SharedGetData
import com.example.osm_map_project.domain.usecase.SaveDataUseCase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module

val repoModule = module {

    fun provideOkHttp(context: Context): OkHttpClient {
        val log = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
        return  OkHttpClient.Builder()
            .addInterceptor(log)
            .addInterceptor(AuthInterceptor(context))
            .build()
    }

    fun provideRetrofit() {

    }

    factory<SharedRepositoryRepo> {
        SharedRepositoryStorage(get())
    }

    factory<SharedGetData> {
        SharedRepositoryImpl(get())
    }

    single {
        SaveDataUseCase(get())
    }

    single {
        provideOkHttp(get())
    }
}