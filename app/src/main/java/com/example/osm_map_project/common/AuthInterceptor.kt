package com.example.osm_map_project.common

import android.content.Context
import com.google.gson.Gson
import com.yandex.mapkit.search.Chain
import io.mockk.InternalPlatformDsl.toStr
import okhttp3.*
import java.nio.file.attribute.AclEntry.newBuilder

class AuthInterceptor(context: Context): Interceptor {
    private val sharedPreferences = context.getSharedPreferences("main", Context.MODE_PRIVATE)

    private fun refreshToken(token: String?): String {
        if (token.isNullOrEmpty()) {
            return "None"
        } else {
            val client = OkHttpClient()

            val formBody = FormBody.Builder()
                .add("Authorization", "Bearer $token")
                .build()

            val requestNew = Request.Builder()
                .url("http://dasdsd.ru")
                .post(formBody)
                .build()

            val result = client.newCall(requestNew).execute()
            if (result.code == 200) {
                val result_token = Gson().fromJson(result.message.toString(), SuccessRefresh::class.java)
                return result_token.refresh_token
            } else {
                return "None"
            }
        }
    }

    private fun generateNewRequest(chain: Interceptor.Chain, token: String): Response {
        val request = chain.request().newBuilder()
            .header("Authorization", token)
            .build()
        return chain.proceed(request)
    }

    private fun getRefreshToken(): String? {
        val refreshToken = sharedPreferences.getString("refresh_token", null)
        return refreshToken
    }

    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()
        val response = chain.proceed(request)

        if (response.code == 401) {
            response.close()
            val token = getRefreshToken()
            val resultRefresh = refreshToken(token)
            if (resultRefresh != "None") {
                return generateNewRequest(chain, resultRefresh)
            }
            return chain.proceed(request)
        } else {
            return chain.proceed(request)
        }
    }
}