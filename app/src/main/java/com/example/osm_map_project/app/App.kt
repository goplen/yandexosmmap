package com.example.osm_map_project.app

import android.app.Application
import com.example.osm_map_project.di.dataModule
import com.example.osm_map_project.di.repoModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(repoModule, dataModule)
        }
    }
}