package com.example.osm_map_project.data.storage

import android.content.Context
import android.content.SharedPreferences


private const val SHARED_MODULE = "main"
private const val KEY_GET_DATA = "login"
class SharedRepositoryStorage(val context: Context): SharedRepositoryRepo {
    val sharedPreferences = context.getSharedPreferences(SHARED_MODULE, Context.MODE_PRIVATE)
    override fun saveData(): Boolean {
        sharedPreferences.edit().putString(KEY_GET_DATA, "scam").apply()
        return true
    }
}